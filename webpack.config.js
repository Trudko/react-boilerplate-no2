const path = require("path");
const webpack = require("webpack");
const DotenvPlugin = require("dotenv-webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const Visualizer = require("webpack-visualizer-plugin");

const PRODUCTION = process.env.NODE_ENV === "production";
const BUILD = PRODUCTION || process.env.NODE_ENV === "local_build";

function getDevTool() {
  return PRODUCTION ? "cheap-module-source-map" : "source-map";
}

function getEntry() {
  const entry = {};

  entry.vendor = [
    "babel-polyfill",
    "bowser",
    "cuid",
    "history",
    "js-cookie",
    "lodash/isEqual",
    "react",
    "react-dom",
    "react-html-parser",
    "react-notification-system",
    "react-redux",
    "react-router",
    "react-router-dom",
    "react-router-redux",
    "reduce-reducers",
    "redux",
    "redux-thunk",
    "reselect",
    "url-search-params-polyfill",
    "whatwg-fetch"
  ];

  if (BUILD) {
    entry.bundle = ["babel-polyfill", "./src/index.js"];
  } else {
    entry.bundle = [
      "babel-polyfill",
      "react-hot-loader/patch",
      "webpack-dev-server/client?http://0.0.0.0:3000",
      "./src/index.js"
    ];
  }
  return entry;
}

function getJSRule() {
  if (BUILD) {
    return {
      test: /\.js[x]?$/,
      loader: "babel-loader",
      exclude: /node_modules/
    };
  } else {
    return {
      test: /\.js[x]?$/,
      use: [
        {
          loader: "babel-loader"
        }
      ],
      include: path.resolve(__dirname, "src")
    };
  }
}

function getCSSRule() {
  if (BUILD) {
    return {
      test: /\.[s]?css$/,
      use: ExtractTextPlugin.extract({
        use: [
          "css-loader",
          "postcss-loader",
          {
            loader: "sass-loader",
            options: {
              includePaths: [path.resolve(__dirname, "./src")]
            }
          }
        ]
      })
    };
  } else {
    return {
      test: /\.[s]?css$/,
      use: [
        "style-loader",
        {
          loader: "css-loader",
          options: {
            sourceMap: true
          }
        },
        {
          loader: "postcss-loader",
          options: {
            sourceMap: true
          }
        },
        {
          loader: "sass-loader",
          options: {
            sourceMap: true,
            includePaths: [path.resolve(__dirname, "./src")]
          }
        }
      ]
    };
  }
}

function getPlugins() {
  const plugins = [
    new webpack.NoEmitOnErrorsPlugin(),
    new HtmlWebpackPlugin({
      template: "./src/index.template.ejs",
      inject: "body"
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: "vendor",
      filename: "vendor.js"
    })
  ];

  if (BUILD) {
    plugins.push(
      new ExtractTextPlugin({
        filename: "style.css",
        allChunks: true
      })
    );
  }
  if (PRODUCTION) {
    plugins.push(
      new webpack.optimize.UglifyJsPlugin({
        sourceMap: true
      })
    );
    plugins.push(
      new DotenvPlugin({
        path: "./.env.production"
      })
    );
  } else {
    plugins.push(
      new DotenvPlugin({
        path: "./.env.dev"
      }),
      new Visualizer()
    );
  }

  return plugins;
}

module.exports = {
  devtool: getDevTool(),
  entry: getEntry(),
  output: {
    path: path.join(__dirname, "public"),
    filename: "[name].js",
    chunkFilename: "[name].[chunkhash].js",
    publicPath: "/"
  },
  module: {
    rules: [
      getJSRule(),
      getCSSRule(),
      {
        test: /\.(png|woff|woff2|eot|ttf|svg)$/,
        loader: "url-loader"
      }
    ]
  },
  node: {
    dns: "empty",
    net: "empty"
  },
  plugins: getPlugins(),
  devServer: {
    historyApiFallback: true,
    disableHostCheck: true,
    proxy: {
      "/api": {
        target: "http://localhost:3005",
        secure: false
      }
    }
  }
};
