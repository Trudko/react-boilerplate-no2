#!/usr/bin/env bash
moduleName=$1
componentName=$2
redux generate component ${moduleName} ${componentName}
git add src/${moduleName}/components/${componentName^}
git add src/${moduleName}/actions/${componentName}.js
git add src/${moduleName}/actionTypes/${componentName}.js
git add src/${moduleName}/reducer/${componentName}.js
git add test/unit/${moduleName}/components/${componentName^}
