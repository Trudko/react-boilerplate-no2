// @flow
import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import <%= componentName %> from "./<%= componentName %>";

type Props = {
};

export class <%= componentName %>Container extends React.Component {
    props: Props;

    render() {
        return <<%= componentName %>/>;
    }
}

const mapStateToProps = (state, ownProps) => ({
    ...ownProps
});

const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(<%= componentName %>Container);
