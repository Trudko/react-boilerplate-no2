import type {StoreState} from "./flowTypes";

export const getNotifications = (state: StoreState) => state.app.notifications;
