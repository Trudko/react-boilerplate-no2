// @flow
export type State = {
    apiError: ?string,
    previousPath: ?string,
    currentPath: ?string,
    notifications: NotificationType[]
};

export type StoreState = {
    app: State
};

export type NotificationType = {
    id: string,
    title?: ?string,
    message: string,
    level: "success" | "error" | "warning" | "info"
};
