// @flow
import {LOCATION_CHANGE} from "react-router-redux";
import reduceReducers from "reduce-reducers";

import * as types from "app/actionTypes";
import type {State} from "app/flowTypes";


export const initialState: State = {
    currentPath: null,
    previousPath: null,
    apiError: null,
    notifications: []
};

export default reduceReducers(
    (state: State = initialState, action: Object) => {
        switch (action.type) {
            case LOCATION_CHANGE: {
                return {
                    ...state,
                    previousPath: state.currentPath === action.payload.pathname ? state.previousPath : state.currentPath,
                    currentPath: action.payload.pathname
                };
            }
            case types.CLEAR_PATHS: {
                return {
                    ...state,
                    previousPath: null,
                    currentPath: null
                };
            }
            case types.API_ERROR: {
                return {
                    ...state,
                    apiError: action.error
                };
            }
            case types.CLEAR_API_ERROR: {
                return {
                    ...state,
                    apiError: null
                };
            }
            case types.ADD_NOTIFICATION: {
                return {
                    ...state,
                    notifications: state.notifications.concat(action.notification)
                };
            }
            case types.REMOVE_NOTIFICATION: {
                return {
                    ...state,
                    notifications: state.notifications.filter(notification => notification.id !== action.notification.id)
                };
            }

            default: {
                return state;
            }
        }
    }
);
