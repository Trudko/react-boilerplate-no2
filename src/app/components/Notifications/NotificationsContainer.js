// @flow
import React from "react";
import ReactHtmlParser from "react-html-parser";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import NotificationSystem from "react-notification-system";
import differenceBy from "lodash/differenceBy";

import {getNotifications} from "app/selectors";

import type {NotificationType} from "app/flowTypes";

import "./notifications.scss";

type Props = {
    notifications: NotificationType[]
};

class NotificationsContainer extends React.Component {
    props: Props;

    componentWillReceiveProps({notifications}: Props) {
        const toBeAddedNotifications = differenceBy(notifications, this.props.notifications, "id");
        const toBeRemovedNotifications = differenceBy(this.props.notifications, notifications, "id");

        toBeAddedNotifications.forEach(this.addNotification);
        toBeRemovedNotifications.forEach(this.removeNotification);
    }

    notificationSystem: NotificationSystem;

    getMessage = (notification: NotificationType) => {
        const pattern = /.*(<.+\/?>)+.*(<\/.+>)*.*/;

        if (notification.message.match(pattern)) {
            return null;
        } else {
            return notification.message;
        }
    };

    getChildren = (notification: NotificationType) => {
        const pattern = /.*(<.+\/?>)+.*(<\/.+>)*.*/;

        if (notification.message.match(pattern)) {
            return ReactHtmlParser(`<div class="notification-message">${notification.message}</div>`);
        } else {
            return null;
        }
    };

    addNotification = (notification: NotificationType) => {
        this.notificationSystem.addNotification({
            uid: notification.id,
            title: notification.title,
            message: this.getMessage(notification),
            children: this.getChildren(notification),
            level: notification.level,
            autoDismiss: 0,
            position: "tr"
        });
    };

    removeNotification = (notification: NotificationType) => {
        this.notificationSystem.removeNotification(notification.id);
    };

    render() {
        return (
            <NotificationSystem ref={notificationSystem => this.notificationSystem = notificationSystem}/>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({
    ...ownProps,
    notifications: getNotifications(state)
});
const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(NotificationsContainer);