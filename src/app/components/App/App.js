// @flow
import React from 'react';
import Button from 'material-ui/Button';

type Props = {
    children: React.Element
};

const App = ({children}: Props) => (
    <div className="app">
        <Button raised color="primary">
            foo
        </Button>
        {children}
    </div>
);

export default App;
