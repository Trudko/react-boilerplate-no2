// @flow
import React from "react";
import {Route} from "react-router";
import {connect} from "react-redux";

import App from "./App";
import {Notifications} from "app/components";

type Props = {};

class AppContainer extends React.Component {
    props: Props;

    render() {
        return (
            <App>
                <Route path="/" component={Notifications}/>
            </App>
        );
    }
}

const mapStateToProps = () => ({});

export default connect(mapStateToProps)(AppContainer);
