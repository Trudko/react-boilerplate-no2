// @flow
import {push} from "react-router-redux";
import cuid from "cuid";

import config from "config";
import * as types from "app/actionTypes";
import type {NotificationType} from "app/flowTypes";

export const dispatchError = (dispatch: Function, error: Object, actionType?: string) => {
    if (error.status) {
        switch (error.status) {
            case 401:
                dispatch(push("/login"));
                return;
        }
    }

    if (config.SHOW_ERROR_DETAILS) {
        console.error(error);  // eslint-disable-line
    }

    dispatch({
        type: actionType || types.API_ERROR,
        error
    });
};

export const clearPaths = () => ({
    type: types.CLEAR_PATHS
});

const showNotification = (message: string, title: ?string, level: "info" | "success" | "warning" | "error", showTime: number) => (dispatch: Function) => {
    const notification: NotificationType = {
        id: cuid(),
        title,
        message,
        level
    };
    dispatch({
        type: types.ADD_NOTIFICATION,
        notification: notification
    });
    setTimeout(() => dispatch(removeNotification(notification)), showTime);
};

export const showInfoNotification = (message: string, title?: ?string = null, showTime?: number = 5000) => showNotification(message, title, "info", showTime);

export const showSuccessNotification = (message: string, title?: ?string = null, showTime?: number = 5000) => showNotification(message, title, "success", showTime);

export const showWarningNotification = (message: string, title?: ?string = null, showTime?: number = 5000) => showNotification(message, title, "warning", showTime);

export const showErrorNotification = (message: string, title?: ?string, showTime?: number = 5000) => showNotification(message, title, "error", showTime);

export const removeNotification = (notification: NotificationType) => ({
    type: types.REMOVE_NOTIFICATION,
    notification
});
