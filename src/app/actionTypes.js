import {NAME} from "./constants";

export const API_ERROR = `${NAME}/API_ERROR`;
export const CLEAR_API_ERROR = `${NAME}/CLEAR_API_ERROR`;
