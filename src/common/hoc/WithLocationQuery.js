import React from "react";

type Props = {
    location?: Object
};

export default (Component) => class WithLocationQuery extends React.Component {
    props: Props;

    getLocationQuery = () => {
        const {location} = this.props;

        if (!location) {
            return {};
        }

        const searchParams = new URLSearchParams(location.search);
        const query = {};

        for (let param of searchParams) {
            query[param[0]] = param[1];
        }

        return query;
    };

    render() {
        return <Component {...this.props} locationQuery={this.getLocationQuery()}/>;
    }
};