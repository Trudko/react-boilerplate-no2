// @flow
import React from "react";

import {Button} from "common/components/ui";

import "./errorMessageOverlay.scss";

type Props = {
    title?: string,
    messages: string[],
    done: Function
};

const ErrorMessageOverlay = ({title, messages, done}: Props) => {
    return (
        <div className="errorMessageOverlay">
            <div className="title">{title || "Error"}</div>
            <ul className="messages">
                {messages.map((message, index) => <li key={index} className="message">{message}</li>)}
            </ul>
            <Button className="doneButton" text="OK" onClick={done}/>
        </div>
    );
};
export default ErrorMessageOverlay;
