// @flow
import React from "react";

import "./loading.scss";

type Props = {
    message?: ?string
};

const Loading = ({message}: Props) => {
    return (
        <div className="loading">
            <div className="progressMessage">
                <img src="/images/loading.gif" alt="loading"/>
                {message && <div className="message">{message}</div>}
            </div>
        </div>
    );
};

export default Loading;
