// @flow
import React from "react";
import {Loading} from "common/components";

export default (importComponent: Function, className?: string) => {
    return class LazyComponent extends React.Component {
        static Component = null;
        state = {Component: LazyComponent.Component};

        componentWillMount() {
            if (!this.state.Component) {
                importComponent()
                    .then(module => module.default)
                    .then(Component => {
                        LazyComponent.Component = Component;
                        if (!this.unmounted) {
                            this.setState({Component});
                        }
                    });
            }
        }

        componentWillUnmount() {
            this.unmounted = true;
        }

        unmounted: boolean = false;

        render() {
            const {Component} = this.state;
            if (Component) {
                return <Component {...this.props}/>;
            }
            return (
                <div className={className}>
                    <Loading size={24}/>
                </div>
            );
        }
    };
};
