// @flow
import "./styles/global.scss";

import "url-search-params-polyfill";
import React from "react";
import {render} from "react-dom";
import {AppContainer} from "react-hot-loader";

import {createStore, combineReducers, applyMiddleware} from "redux";

import {Route} from "react-router";
import {ConnectedRouter, routerReducer, routerMiddleware} from "react-router-redux";
import {Provider} from "react-redux";
import thunk from "redux-thunk";
import reducers from "./rootReducer";
import createHistory from "history/createHashHistory";

import App from "./app/components/App/App";

const history = createHistory();
const store = createStore(
    combineReducers({
        ...reducers,
        routing: routerReducer
    }),
    String(process.env) !== "production" ? window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() : {},
    applyMiddleware(thunk, routerMiddleware(history))
);

render(
    <Provider store={store}>
        <AppContainer>
            <ConnectedRouter history={history}>
                <Route path="/" component={App}/>
            </ConnectedRouter>
        </AppContainer>
    </Provider>,
    document.getElementById("root")
);

if (module.hot) {
    module.hot.accept();

    module.hot.dispose((data) => {
        data.store = store;
    });
}
